public class funcs {
    static String word(int a, String[] words) {
        int word = 0;
        int n = a;
        if (n > 99)
            n = toTwo(n);

        if (n >= 21 && n <= 99)
            n = toOne(n);

        if (n >= 10 && n <= 20 || n >= 5 || n == 0)
            word = 2;
        else if (n >= 2 && n <= 4)
            word = 1;

        return a + " " + words[word];
    }

    static int toTwo(int a) {
        String s = Integer.toString(a);
        a = Integer.parseInt(s.substring(s.length() - 2));
        return a;
    }

    static int toOne(int a) {
        String s = Integer.toString(a);
        a = Integer.parseInt(s.substring(s.length() - 1));
        return a;
    }

}
