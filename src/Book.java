import java.util.Random;

public class Book {
    int year;
    String authorName, authorLastName, title;

    Book() {
    }

    Book(String name) {
        this.title = name;
    }

    Book(String title, String authorLastName, String authorName, int year) {
        this.year = year;
        this.authorName = authorName;
        this.authorLastName = authorLastName;
        this.title = title;
    }

    Book(String authorLastName, String authorName, int year) {
        this.year = year;
        this.authorName = authorName;
        this.authorLastName = authorLastName;
    }

    static Book createBook() {
        Random rnd = new Random();
        String[] names = {"Peter", "Ivan", "Max", "Mary", "Lora", "Inna", "Irakly", "Kate", "Roman", "David", "Lina"};
        String[] lastNames = {"Parker", "Watson", "Klinton", "Gandelman", "Wasserman", "Wayne", "Clark", "Lazar"};
        Book book = new Book(lastNames[rnd.nextInt(lastNames.length)], names[rnd.nextInt(names.length)], rnd.nextInt((2018 - 1980) + 1) + 1980);
        book.bookNameGenerator();

        return book;
    }

    static Book[] createBooks(int a) {
        Book[] books = new Book[a];
        for (int i = 0; i < a; i++)
            books[i] = createBook();
        return books;
    }

    static void search(String s, Book[] books) {
        int[] list = new int[books.length];
        int k = 0;
        String[] bookNameParts;
        String[] searchWords = s.split(" ");
        for (int i = 0; i < books.length; i++) {
            bookNameParts = books[i].title.split(" ");
            boolean flag = false;
            for (int j = 0; j < searchWords.length; j++) {
                for (int l = 0; l < bookNameParts.length; l++) {
                    if (searchWords[j].equals(books[i].authorLastName) || searchWords[j].equals(books[i].authorName) || searchWords[j].equals(bookNameParts[l])) {
                        list[k] = i;
                        k++;
                        flag = true;
                        break;
                    }
                }
                if (flag) {
                    break;
                }
            }
        }
        Book[] findBooks = new Book[k];
        if (k != 0) {
            for (int i = 0; i < k; i++) {
                findBooks[i] = new Book();
                findBooks[i].clone(books[list[i]]);
            }
            printBooks(findBooks);
            System.out.println("\n" + funcs.word(k, new String[]{"книга найдена", "книги найдено", "книг найдено"}) + " по поисковому запросу \"" + s + "\".");
        } else
            System.out.println("\nКниги по поисковому запросу \"" + s + "\" не найдены.");
    }

    static void search(int s, Book[] books) {
        int[] list = new int[books.length];
        int k = 0;
        for (int i = 0; i < books.length; i++) {
            if (s == books[i].year) {
                list[k] = i;
                k++;
                break;
            }
        }
        Book[] findBooks = new Book[k];
        if (k != 0) {
            for (int i = 0; i < k; i++) {
                findBooks[i] = new Book();
                findBooks[i].clone(books[list[i]]);
            }
            printBooks(findBooks);
            System.out.println("\n" + funcs.word(k, new String[]{"книга найдена", "книги найдено", "книг найдено"}) + " с годом издания \"" + s + "\".");
        } else
            System.out.println("\nКниги с годом издания \"" + s + "\" не найдены.");
    }

    void bookNameGenerator() {
        String res = "";
        NameGenerator gnrtr = new NameGenerator();
        Random rnd = new Random();
        String s = gnrtr.generateName(2 + rnd.nextInt(1));
        switch (rnd.nextInt(3)) {
            case 0: {
                res = "The last " + s;
                break;
            }
            case 1: {
                res = s + "'s World";
                break;
            }
            case 2: {
                res = s;
                break;
            }
        }
        title = res;
    }

    void clone(Book a) {
        year = a.year;
        authorName = a.authorName;
        authorLastName = a.authorLastName;
        title = a.title;
    }

    void showInfo() {
        String author = authorLastName + " " + authorName;
        System.out.printf("%18s  |\t\t%15s |\t\t%8d  \n", this.title, author, this.year);
    }

    static void printBooks(Book[] books) {
        String ANSI_BOLD = "\u001B[1m";
        String ANSI_RESET = "\u001B[0m";
        System.out.println(ANSI_BOLD + "\tНазвание книги  |\t\t    Имя автора  |\tГод выпуска" + ANSI_RESET);
        for (int i = 0; i < books.length; i++) {
            books[i].showInfo();
        }
    }

    static Book[] deleteBook(String s, Book[] books) {
        Book[] newBooks = new Book[books.length - 1];
        for (int i = 0; i < books.length; i++) {
            if (s.equals(books[i].title)) {
                for (int j = i + 1; j < books.length; j++) {
                    if (j != books.length - 1)
                        books[j - 1].clone(books[j]);
                    else {
                        books[j] = null;
                    }
                }
                break;
            }
        }
        for (int i = 0; i < newBooks.length; i++) {
            newBooks[i] = new Book();
            newBooks[i].clone(books[i]);
        }
        return newBooks;
    }

    static Book[] addBook(String title, String authorLastName, String authorName, int year, Book[] books) {
        Book[] newBooks = new Book[books.length + 1];
        for (int i = 0; i < books.length; i++) {
            newBooks[i] = new Book();
            newBooks[i].clone(books[i]);
        }
        newBooks[newBooks.length - 1] = new Book(title, authorLastName, authorName, year);
        return newBooks;
    }

}
