import java.util.Random;

public class NameGenerator {
    String[] Beginning = {"Kr", "C", "R", "M", "Cr",
            "Dr", "Br", "Z", "D", "J", "Jun", "Jar", "Mol",
            "Zor", "Rag", "Nar", "Zur", "B", "Azk", "Az", "Ad", "Pr",
            "Mar", "L"};
    String[] Middle = {"aire", "ir", "omi", "ora", "elo",
            "ala", "oro", "ara", "ardo", "omiri", "alori", "ucre", "umuro", "azero",
            "amara", "azoiru", "ura"};
    String[] End = {"d", "ed", "ark", "lar", "sar", "dar", "der",
            "tron", "med", "rud", "bad", "dud", "mur"};

    Random rnd = new Random();

    String generateName(int a) {
        String res = "";
        if (a < 3) {
            switch (a) {
                case 1: {
                    res += Beginning[rnd.nextInt(Beginning.length)];
                    break;
                }
                case 2: {
                    res += Beginning[rnd.nextInt(Beginning.length)] + Middle[rnd.nextInt(Middle.length)];
                    break;
                }
            }
        } else {
            res += Beginning[rnd.nextInt(Beginning.length)];
            for (int i = 0; i < a - 2; i++) {
                res += Middle[rnd.nextInt(Middle.length)];
            }
            res += End[rnd.nextInt(End.length)];
        }
        return res;
    }
}
