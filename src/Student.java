import java.util.Random;

public class Student {
    String lastName, name;
    int groupNumber, grade;

    Student() {
    }

    Student(String name) {
        this.name = name;
    }

    public Student(String lastName, String name, int groupNumber, int grade) {
        this.lastName = lastName;
        this.name = name;
        this.groupNumber = groupNumber;
        this.grade = grade;
    }

    void showInfo() {
        System.out.printf("%7s  |\t\t%9s  |\t\t%8d  |\t\t%2d\n", this.name, this.lastName, this.groupNumber, this.grade);
    }

    static void printStudents(Student[] students) {
        String ANSI_BOLD = "\u001B[1m";
        String ANSI_RESET = "\u001B[0m";
        System.out.println(ANSI_BOLD + "\tИмя  |\t\t  Фамилия  |\tНомер группы  |\tОценка" + ANSI_RESET);
        for (int i = 0; i < students.length; i++) {
            students[i].showInfo();
        }
    }

    void clone(Student a) {
        lastName = a.lastName;
        name = a.name;
        groupNumber = a.groupNumber;
        grade = a.grade;
    }

    static Student[] initStudents(int a) {
        Random rnd = new Random();
        String[] names = {"Peter", "Ivan", "Max", "Mary", "Lora", "Inna", "Irakly", "Kate", "Roman", "David", "Lina"};
        String[] lastNames = {"Parker", "Watson", "Klinton", "Gandelman", "Wasserman", "Wayne", "Clark", "Lazar"};
        int[] groupNumbers = {14, 78, 22, 420};
        Student[] students = new Student[a];
        for (int i = 0; i < a; i++) {
            int r = rnd.nextInt(5) + 1;
            r = r < 2 ? 2 : r;
            students[i] = new Student(lastNames[rnd.nextInt(lastNames.length)], names[rnd.nextInt(names.length)], groupNumbers[rnd.nextInt(groupNumbers.length)], r);
        }
        return students;
    }

    static void showGoodTalmidim(Student[] students) {
        int k = 0;
        int[] list = new int[students.length];

        for (int i = 0; i < students.length; i++) {
            if (students[i].grade > 3) {
                list[k] = i;
                k++;
            }
        }
        if (k != 0) {
            Student[] goodTalmidim = new Student[k];
            for (int i = 0; i < k; i++) {
                goodTalmidim[i] = new Student();
                goodTalmidim[i].clone(students[list[i]]);
            }
            sortByGrade(goodTalmidim);
            printStudents(goodTalmidim);
            System.out.println("\n" + funcs.word(k, new String[]{"студент найден", "студента найдено", "студентов найдено"}) + " с оценками 4 или 5.");
        } else
            System.out.println("\nХорошистов и отличников нет :(");
    }

    static void search(String s, Student[] students) {
        int[] list = new int[students.length];
        int k = 0;
        for (int i = 0; i < students.length; i++) {
            if (s.equals(students[i].name) || s.equals(students[i].lastName)) {
                list[k] = i;
                k++;
            }
        }
        Student[] findStudents = new Student[k];
        if (k != 0) {
            for (int i = 0; i < k; i++) {
                findStudents[i] = new Student();
                findStudents[i].clone(students[list[i]]);
            }
            printStudents(findStudents);
            System.out.println("\n" + funcs.word(k, new String[]{"студент найден", "студента найдено", "студентов найдено"}) + " по поисковому запросу \"" + s + "\".\n");
        } else
            System.out.println("\nСтуденты с именем или фамилией \"" + s + "\" не найдены.\n");
    }

    static void sortByGrade(Student[] students) {
        int max, max_i;
        Student tmp = new Student();
        for (int i = 0; i < students.length; i++) {
            max = students[i].grade;
            max_i = -1;
            for (int j = i + 1; j < students.length; j++) {
                if (max < students[j].grade) {
                    max = students[j].grade;
                    max_i = j;
                }
            }
            if (max_i != -1) {
                tmp.clone(students[max_i]);
                students[max_i].clone(students[i]);
                students[i].clone(tmp);
            }
        }
    }
}