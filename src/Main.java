public class Main {
    public static void main(String[] args) {
        /*      Работа с классом Student      */

        Student[] students = Student.initStudents(10); // Инициализируем массив из 10 студентов

        Student.search("Parker", students); // Поиск и вывод студентов с именем или фамилией "Parker"

        Student.printStudents(students); // Напечатать список всех студентов и их полей
        System.out.println();

        Student.sortByGrade(students); // Сортировка списка студентов по оценке

        Student.printStudents(students);
        System.out.println();

        Student.showGoodTalmidim(students); // Напечатать список всех студентов, которые имеют оценки 4 или 5
        System.out.println();

        /*      Работа с классом Book      */

        Book[] books = Book.createBooks(10);

        books[3].title = "Swag";

        Book.printBooks(books);
        System.out.println();

        books = Book.deleteBook("Swag", books);

        Book.printBooks(books);
        System.out.println();

        Book.search("World", books);

        books = Book.addBook("Swag", "Bobrov", "David", 2017, books);

        Book.printBooks(books);
        System.out.println();
    }
}
